const SECURITY_NAME: &str = "authToken";

pub mod builder;
pub mod handler;
pub mod operation;
pub mod router;
